# proposition

## objectif
co-écrire une proposition pour la conférence JRES2024

## TODO
- [x] créer un lieu de partage d'écriture et de lecture
- [x] pousser le texte initial
- [ ] échanger/modifier la proposition (500 à 1300 mots) pour les contributeur-ices
- [ ] identifier le thème (souveraineté, sobriété, enjeux sociétaux métiers et organisations, IA et nouveaux usages, mode sans échec, sustèmes et réseaux, sécurité, stratégie numérique des établissements et laboratoire soutien à la recherche, usages applications et identités numériques)
- [ ] identifier le format (présentation longue, courte, poster, tuto long, court)
- [ ] créer la bibliographie
- [ ] déposer la proposition sur la plateforme JRES2024 (https://conf-ng.jres.org/2024/) avant le 8 mars
