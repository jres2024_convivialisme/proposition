## préambule (épistolaire)

- lors de l'ANF Ecoinfo a été évoquée l'idée de proposer un article JRES autour du contenu de mon intervention (version longue de la présentation faite aux JRAF https://www.youtube.com/watch?v=cy_FKLnz40I).  
- lors des JRAF, Sophie m'avais dit qu'elle écrirait bien qqchose autour de l'échange que nous avions eu.  
- un peu plus tard dans la semaine, je rencontrais Sylvain et je faisais le lien entre les deux premiers points: peut-être devrions nous co-écrire (au moins à 3) cet article JRES.  
- toujours cette semaine, Françoise m'embarquait dans la mise en place d'une formation. J'ai accepté bien volontiers puisque certains livrables font écho à d'autres échanges (surtout: une vidéo de présentation sur le convivialisme et l'informatique post-personnelle).  
- lors d'une visite strasbourgeoise, Raphaëlle acceptait de prendre en charge le côté sciences humaines.  
- je comptais faire part de tout cela à Sophie pour participer à la rédaction de ces idées  
- il y a pas mal de redondance entre le contenu d'un article JRES et ceux pour lesquels je me suis engagé par ailleurs.  
- la DNUM me paye le ticket pour JRES sur la base de l'abstract qui suit.  
- mon plan se divise en 4 points dont le 2e pourrait être un moment fort dans notre communauté :  
	- état actuel (faisons confiance à l'équipe de Françoise et Ecoinfo pour ça)  
	- appel à un changement radical que nous pourrions co-écrire et publier largement avant les JRES pour y apposer les signatures d'un maximum de membres de la communauté ESR (voir plus?). Je pense qu'on a au moins 3 DSI dans les possible signataires.  
	- proposition d'un numérique soutenable: post-personnel, convivial, inclusif utilisant les principes de la philo unix comme base de réflexion.  
	- proposer l'éco-conception comme l'ensemble des moyens structurels, organisationnels, pédagogiques et techniques rendre possible une transition entre le numérique actuel et notre proposition.

## une vision politique de l'éco-conception

Alors que les alertes écologiques se multiplient et que la proximité des limites planétaires ont une incidence de plus en plus directe sur nos finances, les stratégies de réduction de l'impact du numérique semblent inefficaces relativement à l'urgence et la gravité de la situation.  

L'éco-conception telle qu'elle nous est proposée par la vision actuelle du numérique est un techno-solutionisme invitant d'une part les producteurs à la recherche d'économies marginales (insuffisantes même si conséquentes), d'une autre les consommateurs à une sobriété d'usage dont ils peinent à définir les enjeux et rendue presque impossible par la nature de la logithèque, inadéquate par rapport au but recherché.

Du côté des "technophiles" au contraire, il existe un regain d'intérêt pour les principes et pratiques qui repoussent radicalement la complexité des systèmes informatiques ses conséquences tant sur la structure technique que sur les communautés d'utilisateurs et de producteurs. Notre rapport au numérique y est intérrogé radicalement jusqu'à questionner la qualité et la nécessité des interactions sociales et techniques rendues possibles par le numérique, les impératifs économiques qui ont conduit aux condition actuelles de sa production, leur adéquation avec un projet de société qui soit à la fois heureux et soutenable pour les générations futures. Sans qu'ils soient nommés, des concepts existant dans la philosophie des sciences (comme le convivialisme de Illich) y sont transmis, redécouverts intuitivement et mobilisés.

Ces concepts se matérialisent par des pratiques sociales et techniques trop éloignées de l'idéologie actuelle. Nos politiques ne disposent pas des imaginaires nécessaires pour comprendre l'intérêt d'ouvrir des voies vers le convivialisme dans les systèmes d'information actuels. Même si cette réflexion semble salutaire pour une partie grandissante de notre communauté, l'impossibilité d'une application immédiate et un manque de confiance dans notre capacité à mobiliser l'intelligence collective en donne une lecture utopique et déconnectée de la réalité.

Dans cet article, nous faisons un retour d'expériences sur les expérimentations et réflexions en cours pour proposer l'éco-conception que nous résumons par ces grands principes:

- le futur du numérique est convivialiste et ~~post-personnel~~ transindividuel, il mobilise les notions de sobriété/frugalité, résilience, déconnexion tant comme des leviers conséquents sur l'impact du numérique que comme comme des contraintes auxquelles nous devrons possiblement faire face (distribution discontinue d'électricité, rupture des chaînes d'approvisionnement, coûts financiers prohibitifs).
- le convivialisme numérique est une pratique collective du numérique qui valorise les interactions sociales pour contenir la complexité des outils, leurs besoins d'évolution, de maintenance et d'infrastructure.
- la démarche convivialiste doit être individuelle, consentie et si possible encouragée par une réorganisation du travail au sein des structures.
- les services numériques doivent permettre cette démarche sans entraver les pratiques actuelles : les deux approches doivent être iso-fonctionnelles, garanties et documentées.
- la vision traditionnelle (et nécessaire) de l'éco-conception doit être complétée par l'ensemble des moyens mis en oeuvre pour rendre effective cette démarche.
